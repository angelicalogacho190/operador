<?php

/**
 *
 */
class Operadoras extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Operadora");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoOperadoras"]=
                    $this->Operadora->consultarTodos();
    $this->load->view("header");
    $this->load->view("operadoras/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id_ope){
    $this->Operadora->eliminar($id_ope);
    $this->session->set_flashdata("confirmacion", "Operadora eliminado existosamente");
    redirect('operadoras/index');
}



  //Renderizando hospitales
  public function nuevo(){
      $this->load->view("header");
      $this->load->view("operadoras/index", $data);
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id_ope){
      $this->load->model("Operadora");
      $data["operadoraEditar"] = $this->Operadora->obtenerPorId($id_ope);
      $this->load->view("header");
      $this->load->view("operadoras/editar", $data);
      $this->load->view("footer");
  }



  //insertar hospitales
  public function guardarOperadora(){
     /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
     $config['upload_path']=APPPATH.'../uploads/Operadoras/'; //ruta de subida de archivos
     $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
     $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
     $nombre_aleatorio="operadora_".time()*rand(100,10000);//creando un nombre aleatorio
     $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
     $this->load->library('upload',$config);//cargando la libreria UPLOAD
     if($this->upload->do_upload("logotipo")){ //intentando subir el archivo
        $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
        $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
     }else{
       $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
     }

    $datosNuevosOperadora=array(
      "id_ope" => $this->input->post("id_ope"),
      "nombre_ope" => $this->input->post("nombre_ope"),
      "direccion_ope" => $this->input->post("direccion_ope"),
      "telefono_ope" => $this->input->post("telefono_ope"),
      "email_ope" => $this->input->post("email_ope"),
      "fechaCreacion_ope" => $this->input->post("fechaCreacion_ope"),
      "logotipo" => $nombre_archivo_subido


    );
    $this->Operadora->insertar($datosNuevosOperadora);
      //flash crear una sesion tipo flash
      $this->session->set_flashdata("confirmacion","Operadora guardado exitosamente");
      redirect('operadoras/index');
  }

  public function actualizarOperadora()
{
 $id_ope = $this->input->post("id_ope");

 // Obtener los datos actuales de la agencia
 $operadoraEditar = $this->Operadora->obtenerPorId($id_ope);

 $nombre_archivo_subido = $operadoraEditar->logotipo; // Por defecto, mantén el nombre actual de la imagen
 if (!empty($_FILES['fotografia_nueva']['name'])) { // Verifica si se ha proporcionado un nuevo archivo de imagen
     // Configuración de la subida de archivo
     $config['upload_path'] = APPPATH . '../uploads/Operadoras/';
     $config['allowed_types'] = 'jpeg|jpg|png';
     $config['max_size'] = 5 * 1024; // 5 MB

     // Genera un nombre aleatorio para el archivo
     $nombre_aleatorio = "operadora_" . time() * rand(100, 10000);
     $config['file_name'] = $nombre_aleatorio;

     // Carga la librería de subida de archivos con la configuración definida
     $this->load->library('upload', $config);

     // Intenta subir el archivo
     if ($this->upload->do_upload("fotografia_nueva")) {
         // Obtén los datos del archivo subido
         $dataArchivoSubido = $this->upload->data();
         // Captura el nombre del archivo subido
         $nombre_archivo_subido = $dataArchivoSubido["file_name"];
     } else {
         // Manejo de errores de subida de archivos aquí si es necesario
         $error = $this->upload->display_errors();
         // Puedes mostrar un mensaje de error o redirigir a una página de error
         // Por ejemplo: $this->session->set_flashdata("error", $error);
         // Luego, redirigir a la página de edición con el mensaje de error
         // Por ejemplo: redirect("revistas/editar/$id");
         return;
     }
 }

 // Obtener los datos actualizados del formulario
 $datosOperadora = array(
    "id_ope" => $this->input->post("id_ope"),
    "nombre_ope" => $this->input->post("nombre_ope"),
    "direccion_ope" => $this->input->post("direccion_ope"),
    "telefono_ope" => $this->input->post("telefono_ope"),
    "email_ope" => $this->input->post("email_ope"),
    "fechaCreacion_ope" => $this->input->post("fechaCreacion_ope"),
    "logotipo" => $nombre_archivo_subido 
 );



 // Actualizar la agencia con los nuevos datos
 $this->Operadora->actualizar($id_ope, $datosOperadora);

 // Flash message
 $this->session->set_flashdata("confirmacion", "Operadora actualizado exitosamente");

 // Redireccionar a la página de lista de agencias
 redirect('operadoras/index');
}





}



 ?>
