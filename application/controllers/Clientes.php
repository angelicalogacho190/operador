<?php

/**
 *
 */
class Clientes extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("Cliente");
    error_reporting(0);

  }
  public function index(){
    // Utiliza una sola función de consulta que traiga la información completa
        $data["listadoClientes"] = $this->Cliente->consultarTodosConOperadora();
        $data["listadoOperadoras"] = $this->Cliente->obtenerListadoOperadoras();
        

        $this->load->view("header");
        $this->load->view("clientes/index", $data);
        $this->load->view("footer");
    }

  public function borrar($id_cli){
    $this->Cliente->eliminar($id_cli);
    $this->session->set_flashdata("confirmacion", "Cliente eliminada existosamente");
    redirect('clientes/index');
}

public function guardarClientes(){
    $datosNuevosCliente = array(
      "id_cli" => $this->input->post("id_cli"),
      "nombre_cli" => $this->input->post("nombre_cli"),
      "apellido_cli" => $this->input->post("apellido_cli"),
      "direccion_cli" => $this->input->post("direccion_cli"),
      "telefono_cli" => $this->input->post("telefono_cli"),
      "email_cli" => $this->input->post("email_cli"),
      "estado_cli" => $this->input->post("estado_cli"),
      "fk_id_ope" => $this->input->post("fk_id_ope")

    );

    $this->Cliente->insertar($datosNuevosCliente);

    // Flash message
    $this->session->set_flashdata("confirmacion","Cliente guardado exitosamente");

    redirect('clientes/index');
}



  public function nuevo(){
      $this->load->view("header");
      $this->load->view("clientes/nuevo");
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id_cli){

      
      $data["listadoOperadoras"] = $this->Cliente->obtenerListadoOperadoras();
      $data["clienteEditar"] = $this->Cliente->obtenerPorId($id_cli);

      $this->load->view("header");
      $this->load->view("clientes/editar", $data);
      $this->load->view("footer");
  }

#aqui me quede ----------------------------------------------------------

public function actualizarCliente(){
    $id_cli = $this->input->post("id_cli");

    // Obtener los datos actuales de la autoría
    $clienteEditar = $this->Cliente->obtenerPorId($id_cli);

    // Obtener los datos actualizados del formulario
    $datosCliente = array(
      "id_cli" => $this->input->post("id_cli"),
      "nombre_cli" => $this->input->post("nombre_cli"),
      "apellido_cli" => $this->input->post("apellido_cli"),
      "direccion_cli" => $this->input->post("direccion_cli"),
      "telefono_cli" => $this->input->post("telefono_cli"),
      "email_cli" => $this->input->post("email_cli"),
      "estado_cli" => $this->input->post("estado_cli"),
      "fk_id_ope" => $this->input->post("fk_id_ope")
    );

    // Actualizar los datos de la autoría en la base de datos
    $this->Cliente->actualizar($id_cli, $datosCliente);

    // Flash message
    $this->session->set_flashdata("confirmacion", "Cliente actualizada exitosamente");

    // Redireccionar a la página de lista de revistas
    redirect('clientes/index');
}


}


 ?>
