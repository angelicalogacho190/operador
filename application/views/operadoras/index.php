<div style="padding: 150px 70px 20px 100px">
    <div class="text-center">
        <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;OPERADORA</h1>
    </div>
    <div class="row">
        <div class="col-md-12 text-end">
            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fa fa-plus-circle fa-1x"></i> Agregar nueva Operadora
            </button>
        </div>
    </div>
    <br>

    <?php if ($listadoOperadoras): ?>
    <table class="table text-center">
        <thead class="table-dark">
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DIRECCIÓN</th>
                <th>TELÉFONO</th>
                <th>EMAIL</th>
                <th>FECHA CREACIÓN</th>
                <th>LOGO TIPO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoOperadoras as $operadora): ?>
            <tr>
            <td class="text-dark"><?php echo $operadora->id_ope; ?></td>
            <td class="text-dark"><?php echo $operadora->nombre_ope; ?></td>
            <td class="text-dark"><?php echo $operadora->direccion_ope; ?></td>
            <td class="text-dark"><?php echo $operadora->telefono_ope; ?></td>
            <td class="text-dark"><?php echo $operadora->email_ope; ?></td>
            <td class="text-dark"><?php echo $operadora->fechaCreacion_ope; ?></td>
            <td class="text-dark">
                <img src="<?php echo base_url('uploads/Operadoras/' . $operadora->logotipo); ?>" alt="Logotipo" width="50" height="50">
            </td>
            <td>
                <a href="<?php echo site_url('operadoras/editar/'.$operadora->id_ope); ?>" class="btn btn-warning" title="Editar">
                    <i class="fa fa-pen"></i>
                </a>
                <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('operadoras/borrar/'.$operadora->id_ope); ?>')">
                    <i class="fa fa-trash"></i>
                </a>
            </td>

            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="modal-footer"></div>

    <?php else: ?>
    <div class="alert alert-danger">
        No se encontró equipo registrado
    </div>
    <?php endif; ?>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nuevo Personal de Operadora</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form class="text-dark" action="<?php echo site_url('operadoras/guardarOperadora') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_operadora">

                        <div class="mb-3 text-dark">
                            <label for="nombre_ope" class="form-label text-dark"><b>Nombre:</b></label>
                            <input id="nombre_ope" type="text" name="nombre_ope" value="" oninput="validarLetras(this)" placeholder="Ingrese el nombre de la operadora" class="form-control" required>
                        </div>
                        <div class="mb-3 text-dark">
                            <label for="direccion_ope" class="form-label text-dark"><b>Dirección:</b></label>
                            <input id="direccion_ope" type="text" name="direccion_ope" value="" oninput="validarLetras(this)" placeholder="Ingrese la direccion" class="form-control" required>
                        </div>
                        <div class="mb-3 text-dark">
                            <label for="telefono_ope" class="form-label text-dark"><b>Teléfono:</b></label>
                            <input id="telefono_ope" type="text" name="telefono_ope" value="09" oninput="validarNumeros(this)" placeholder="Ingrese el telefono" class="form-control" required>
                        </div>
                        <div class="mb-3 text-dark">
                            <label for="email_ope" class="form-label text-dark"><b>Email:</b></label>
                            <input id="email_ope" type="email" name="email_ope" value=""  placeholder="Ingrese el email" class="form-control" required>
                        </div>
                        <div class="mb-3 text-dark">
                            <label for="fechaCreacion_ope" class="form-label text-dark"><b>Fecha Creación:</b></label>
                            <input id="fechaCreacion_ope" type="date" name="fechaCreacion_ope" value=""  placeholder="Ingrese la fecha de creacion" class="form-control" required>
                        </div>
                        <div class="mb-3">
                            <label for="logotipo" class="form-label"><b>Logo:</b></label>
                            <input type="file" id="logotipo" name="logotipo" accept="image/*"  data-browse-on-zone-click="true" class="form-control">
                         </div>
                         <script>
                            $(document).ready(function () {
                                $("#logotipo").fileinput({
                                    //showUpload:false
                                    //showRemove: false,
                                    language:'es',
                                        });
                                    });
                             </script>


                        
                        <div class="row justify-content-end">
                            <div class="col-auto">
                                <button type="submit" name="button" class="btn btn-success">
                                    <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                                </button>
                            </div>
                            <div class="col-auto">
                                <a class="btn btn-danger" href="<?php echo site_url('operadoras/index') ?>">
                                    <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function eliminarRegistro(url) {
    Swal.fire({
        title: '¿Estás seguro de eliminar este registro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, elimínalo!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
            window.location.href = url;
        } else {
            // Si el usuario cancela, mostramos un mensaje de cancelación
            Swal.fire(
                'Cancelado',
                'Tu registro no ha sido eliminado :P',
                'error'
            );
        }
    });
}

function validarLetras(input) {
    input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');

}

function validarNumeros(input) {
    input.value = input.value.replace(/\D/g, '');
}
</script>

<style media="screen">
    input {
        color: black !important;
        font-family: 'Arial';
    }
    tr{
        font-family: 'Arial' !important;
    }
</style>

<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_operadora").validate({
        rules: {
            "nombre_ope": {
                required: true,
                minlength: 4,
                maxlength: 50,

            },

            "direccion_ope": {
              required: true,
              minlength: 3,
              maxlength: 30
            },

            "telefono_ope": {
              required: true,
              minlength: 10,
              maxlength: 10

            },

            "email_ope": {
              required: true,
              email: true

            },

            "fechaCreacion_ope": {
              required: true
              

            },

            "logotipo": {
              required: true,
              

            }
        },
        messages: {

            "nombre_ope": {
                required: "Debe ingresar el nombre",
                minlength: "El nombre debe tener minimo 4 caracteres",
                maxlength: "El nombre no puede tener mas de 50 caracteres"

            },
            "direccion_ope": {
              required: "Debe ingresar la direccion",
              minlength: "La direccion debe tener minimo 3 caracteres",
              maxlength: "La direccion no puede tener mas de 30 caracteres"

            },
            "telefono_ope": {
              required: "Por favor, ingrese el telefono",
              minlength: "El telefono debe tener minimo 10 caracteres",
              maxlength: "El telefono no puede tener mas de 10 caracteres"
            },

            "email_ope": {
              required: "Por favor, ingrese el email",
              email: "Por favor, ingresa un correo electrónico válido"

            },

            "fechaCreacion_ope": {
              required: "Por favor, ingrese la fecha creacion"
            },

            "logotipo": {
            required: "Por favor, ingrese el logo "
            }




        }
    });
});


</script>

