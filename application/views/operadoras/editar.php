<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR OPERADORA
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('operadoras/actualizarOperadora'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_operadora">

        <input type="hidden" value="<?php echo $operadoraEditar->id_ope; ?>" name="id_ope" id="id_ope">
        
        <div class="mb-3 text-dark">
            <label for="nombre_ope" class="form-label text-dark"><b>Nombre:</b></label>
            <input id="nombre_ope" type="text" name="nombre_ope" value="<?php echo $operadoraEditar->nombre_ope; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre de la operadora" class="form-control" required>
        </div>


        <div class="mb-3 text-dark">
            <label for="direccion_ope" class="form-label text-dark"><b>Dirección:</b></label>
            <input id="direccion_ope" type="text" name="direccion_ope" value="<?php echo $operadoraEditar->direccion_ope; ?>" oninput="validarLetras(this)" placeholder="Ingrese la direccion" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="telefono_ope" class="form-label text-dark"><b>Teléfono:</b></label>
            <input id="telefono_ope" type="text" name="telefono_ope" value="<?php echo $operadoraEditar->telefono_ope; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el telefono" class="form-control" required>
        </div>
            <div class="mb-3 text-dark">
            <label for="email_ope" class="form-label text-dark"><b>Email:</b></label>
            <input id="email_ope" type="email" name="email_ope" value="<?php echo $operadoraEditar->email_ope; ?>"  placeholder="Ingrese el email" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="fechaCreacion_ope" class="form-label text-dark"><b>Fecha Creación:</b></label>
            <input id="fechaCreacion_ope" type="date" name="fechaCreacion_ope" value="<?php echo $operadoraEditar->fechaCreacion_ope; ?>"  placeholder="Ingrese la fecha de creacion" class="form-control" required>
        </div>
         
        <label for=""><b>Logo actual de la operadora:</b></label><br>
           <?php if (!empty($operadoraEditar->logotipo)) : ?>
               <img src="<?php echo base_url('uploads/Operadoras/' . $operadoraEditar->logotipo); ?>" alt="Imagen de la operadora" width="200"><br>
           <?php else: ?>
               <p>No hay logo disponible</p>
           <?php endif; ?>
        <br>
         <label for=""><b>Nuevo logo de operadora:</b></label><br>

         <input type="file" name="fotografia_nueva" id="fotografia_nueva" accept="image/*" class="form-control">





        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('operadoras/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');


}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>
<script>
  $(document).ready(function () {
    $("#fotografia_nueva").fileinput({
      //showUpload:false
      //showRemove: false,
      language:'es',
    });
  });
</script>


<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_operadora").validate({
        rules: {
           "nombre_ope": {
                required: true,
                minlength: 4,
                maxlength: 50,

            },

            "direccion_ope": {
              required: true,
              minlength: 3,
              maxlength: 30
            },

            "telefono_ope": {
              required: true,
              minlength: 10,
              maxlength: 10

            },

            "email_ope": {
              required: true,
              email: true

            },

            "fechaCreacion_ope": {
              required: true
              

            },

            "logotipo": {
              required: true,
              

            }
        },
        messages: {

            "nombre_ope": {
                required: "Debe ingresar el nombre",
                minlength: "El nombre debe tener minimo 4 caracteres",
                maxlength: "El nombre no puede tener mas de 50 caracteres"

            },
            "direccion_ope": {
              required: "Debe ingresar la direccion",
              minlength: "La direccion debe tener minimo 3 caracteres",
              maxlength: "La direccion no puede tener mas de 30 caracteres"

            },
            "telefono_ope": {
              required: "Por favor, ingrese el telefono",
              minlength: "El telefono debe tener minimo 10 caracteres",
              maxlength: "El telefono no puede tener mas de 10 caracteres"
            },

            "email_ope": {
              required: "Por favor, ingrese el email",
              email: "Por favor, ingresa un correo electrónico válido"

            },

            "fechaCreacion_ope": {
              required: "Por favor, ingrese la fecha creacion"
            },

            "logotipo": {
            required: "Por favor, ingrese el logo "
            }


        }
    });
});


</script>
<style media="screen">
    input {
        color: black !important;
    }
</style>
