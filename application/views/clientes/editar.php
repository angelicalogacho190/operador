<div style="padding: 150px 100px 20px 100px">
<h1>
<b>
  <i class="fa fa-plus-circle"></i>
  EDITAR CLIENTE
</b>
</h1>
<br>

<form class="" action="<?php echo site_url('clientes/actualizarCliente'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_cliente">


      <div class="mb-3 text-dark">
      <label for="fk_id_ope"><b> Operadora:</b></label>
      <select name="fk_id_ope" id="fk_id_ope" class="form-control" required>
          <option value="">--Seleccione la Operadora--</option>
          <?php foreach ($listadoOperadoras as $operadora): ?>
            <option value="<?php echo $operadora->id_ope; ?>" <?php if ($operadora->id_ope == $clienteEditar->fk_id_ope) echo "selected"; ?>>
            <?php echo $operadora->nombre_ope; ?>
          <?php endforeach; ?>
      </select>
        </div>

        
	<input type="hidden" value="<?php echo $clienteEditar->id_cli; ?>" name="id_cli" id="id_cli">
    <div class="mb-3 text-dark">
        <label for="nombre_cli" class="form-label text-dark"><b>Nombre:</b></label>
        <input id="nombre_cli" type="text" name="nombre_cli" value="<?php echo $clienteEditar->nombre_cli; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre del cliente" class="form-control" required>
    </div>
    <div class="mb-3 text-dark">
        <label for="apellido_cli" class="form-label text-dark"><b>Apellido:</b></label>
        <input id="apellido_cli" type="text" name="apellido_cli" value="<?php echo $clienteEditar->apellido_cli; ?>" oninput="validarLetras(this)" placeholder="Ingrese el apellido" class="form-control" required>
    </div>
    <div class="mb-3 text-dark">
        <label for="direccion_cli" class="form-label text-dark"><b>Dirección:</b></label>
        <input id="direccion_cli" type="text" name="direccion_cli" value="<?php echo $clienteEditar->direccion_cli; ?>" oninput="validarLetras(this)" placeholder="Ingrese la dirección" class="form-control" required>
    </div>
    <div class="mb-3 text-dark">
        <label for="telefono_cli" class="form-label text-dark"><b>Teléfono:</b></label>
        <input id="telefono_cli" type="text" name="telefono_cli" value="<?php echo $clienteEditar->telefono_cli; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el telefono" class="form-control" required>
    </div>
    <div class="mb-3 text-dark">
        <label for="email_cli" class="form-label text-dark"><b>Email:</b></label>
        <input id="email_cli" type="email" name="email_cli" value="<?php echo $clienteEditar->email_cli; ?>"  placeholder="Ingrese el email" class="form-control" required>
    </div>
    <div class="mb-3">
      <label for="estado_cli" class="form-label"><b>Estado:</b></label>
      <select id="estado_cli" name="estado_cli" class="form-control" required>
          <option value="">Seleccione el Estado</option>
          <option value="Activo" <?php if ($clienteEditar->estado_cli == "Activo") echo "selected"; ?>>Activo</option>
          <option value="Inactivo" <?php if ($clienteEditar->estado_cli == "Inactivo") echo "selected"; ?>>Inactivo</option>
      </select>
    </div>

<br>
<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
    &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="btn btn-danger" href=" <?php echo site_url('clientes/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
  </div>

</div>

</form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');


}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}



</script>
<script>
      $(document).ready(function() {
        $('#fk_id_ope').select2({
          placeholder: "--Seleccione la operadora--", // Texto del placeholder
          allowClear: true // Opcional, para permitir que se limpie la selección
        });
      });
</script>
      


<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_cliente").validate({
        rules: {
          "fk_id_ope": {
                    required: true

                },
                
                "nombre_cli": {
                    required: true,
                    minlength: 4,
                    maxlength: 15,

                },
                "apellido_cli": {
                    required: true,
                    minlength: 4,
                    maxlength: 15,

                },
                "direccion_cli": {
                    required: true,
                    minlength: 4,
                    maxlength: 20,
                    

                },
                "telefono_cli": {
                    required: true,
                    minlength: 10,
                    maxlength: 10,

                },
                "email_cli": {
                    required: true,
                    email: true

                },
                "estado_cli": {
                    required: true

                }
              },
              messages: {
                "fk_id_ope": {
                    required: "Escoja la operadora"

                },
                "nombre_cli": {
                  required: "Debe ingresar el nombre del cliente",
                  minlength: "El nombre debe tener minimo 4 caracteres",
                  maxlength: "El nombre no puede tener mas de 15 caracteres"

                },
                "apellido_cli": {
                  required: "Debe ingresar el apellido",
                  minlength: "El apellido debe tener minimo 4 caracteres",
                  maxlength: "El apellido no puede tener mas de 15 caracteres"

                },
                "direccion_cli": {
                  required: "Debe ingresar la dirección",
                  minlength: "La dirección debe tener minimo 4 caracteres",
                  maxlength: "La dirección no puede tener mas de 20 caracteres"


                },
                "telefono_cli": {
                  required: "Por favor, ingrese el telefono",
                  minlength: "El telefono debe tener minimo 10 caracteres",
                  maxlength: "El telefono no puede tener mas de 10 caracteres"
                },
                "email_cli": {
                  required: "Por favor, ingrese el email",
                  email: "Por favor, ingresa un correo electrónico válido"

                },
                "estado_cli": {
                  required: "Debe ingresar el estado del cliente",


                }




        }
    });
});


</script>
<style media="screen">
    input {
        color: black !important;
    }
</style>
