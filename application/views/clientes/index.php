<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
         <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;Cliente</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar nuevo Cliente
      </button>


    </div>

  </div><br>


  <?php if ($listadoClientes): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>NOMBRE</th>
            <th>APELLIDO</th>
            <th>DIRECCIÓN</th>
            <th>TELÉFONO</th>
            <th>EMAIL</th>
            <th>ESTADO</th>
            <th>OPERADORA</th>
            <th>ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoClientes as $cliente): ?>
            <tr>
                <td class="text-dark"><?php echo $cliente->id_cli; ?></td>
                <td class="text-dark"><?php echo $cliente->nombre_cli; ?></td>
                <td class="text-dark"><?php echo $cliente->apellido_cli; ?></td>
                <td class="text-dark"><?php echo $cliente->direccion_cli; ?></td>
                <td class="text-dark"><?php echo $cliente->telefono_cli; ?></td>
                <td class="text-dark"><?php echo $cliente->email_cli; ?></td>
                <td class="text-dark"><?php echo $cliente->estado_cli; ?></td>
                <td class="text-dark"><?php echo $cliente->nombre_ope; ?></td>
                <td>
                  <a href="<?php echo site_url('clientes/editar/'.$cliente->id_cli); ?>" class="btn btn-warning" title="Editar">
                    <i class="fa fa-pen"></i>
                  </a>
                  <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('clientes/borrar/'.$cliente->id_cli); ?>')">
                    <i class="fa fa-trash"></i>
                  </a>

                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro cliente registrados
          </div>
  <?php endif; ?>
</div>


      <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"> Nuevo Cliente
                  </h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
      <div class="container">
          <form  class="text-dark" action="<?php echo site_url('clientes/guardarClientes') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_cliente">
            <div class="mb-3 text-dark">
              <label for="fk_id_ope"><b>Operadora:</b></label>
                  <select name="fk_id_ope" id="fk_id_ope" class="form-control" required>
                      <option value="">--Seleccione la Operadora--</option>
                      <?php foreach ($listadoOperadoras as $operadora): ?>
                          <option value="<?php echo $operadora->id_ope; ?>"><?php echo $operadora->nombre_ope; ?></option>
                      <?php endforeach; ?>
                  </select>
                  </div>
                  


            <div class="mb-3 text-dark">
                <label for="nombre_cli" class="form-label text-dark"><b>Nombre:</b></label>
                <input id="nombre_cli" type="text" name="nombre_cli" value="" oninput="validarLetras(this)" placeholder="Ingrese el nombre del cliente" class="form-control" required>
            </div>
              <div class="mb-3 text-dark">
                  <label for="apellido_cli" class="form-label text-dark"><b>Apellido:</b></label>
                  <input id="apellido_cli" type="text" name="apellido_cli" value="" oninput="validarLetras(this)" placeholder="Ingrese el apellido" class="form-control" required>
              </div>
              <div class="mb-3 text-dark">
                  <label for="direccion_cli" class="form-label text-dark"><b>Dirección:</b></label>
                  <input id="direccion_cli" type="text" name="direccion_cli" value="" oninput="validarLetras(this)" placeholder="Ingrese la dirección" class="form-control" required>
              </div>
              <div class="mb-3 text-dark">
                  <label for="telefono_cli" class="form-label text-dark"><b>Teléfono:</b></label>
                  <input id="telefono_cli" type="text" name="telefono_cli" value="09" oninput="validarNumeros(this)" placeholder="Ingrese el telefono" class="form-control" required>
             </div>
             <div class="mb-3 text-dark">
                  <label for="email_cli" class="form-label text-dark"><b>Email:</b></label>
                  <input id="email_cli" type="email" name="email_cli" value=""  placeholder="Ingrese el email" class="form-control" required>
             </div>
              <div class="mb-3">
                <label for="estado_cli" class="form-label"><b>Estado:</b></label>
                <select id="estado_cli" name="estado_cli" class="form-control" required>
                    <option value="">Seleccione el Estado</option>
                    <option value="Activo">Activo</option>
                    <option value="Inactivo">Inactivo</option>
                </select>
              </div>

              <div class="row justify-content-end">
                  <div class="col-auto">
                      <button type="submit" name="button" class="btn btn-success">
                          <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                      </button>
                  </div>
                  <div class="col-auto">
                      <a class="btn btn-danger" href="<?php echo site_url('clientes/index') ?>">
                          <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                      </a>
                  </div>
              </div>
          </form>
      </div>
  </div>

  </div>
  </div>
    </div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');


      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }
      

     

      </script>
      <script>
            $(document).ready(function() {
              $('#fk_id_ope').select2({
                placeholder: "--Seleccione la operadora--", // Texto del placeholder
                allowClear: true // Opcional, para permitir que se limpie la selección
              });
            });
            </script>
           


      <script type="text/javascript">
      $(document).ready(function() {
          $("#frm_nuevo_cliente").validate({
              rules: {
                "fk_id_ope": {
                    required: true

                },
                
                "nombre_cli": {
                    required: true,
                    minlength: 4,
                    maxlength: 15,

                },
                "apellido_cli": {
                    required: true,
                    minlength: 4,
                    maxlength: 15,

                },
                "direccion_cli": {
                    required: true,
                    minlength: 4,
                    maxlength: 20,
                    

                },
                "telefono_cli": {
                    required: true,
                    minlength: 10,
                    maxlength: 10,

                },
                "email_cli": {
                    required: true,
                    email: true

                },
                "estado_cli": {
                    required: true

                }
              },
              messages: {
                "fk_id_ope": {
                    required: "Escoja la operadora"

                },
                "nombre_cli": {
                  required: "Debe ingresar el nombre del cliente",
                  minlength: "El nombre debe tener minimo 4 caracteres",
                  maxlength: "El nombre no puede tener mas de 15 caracteres"

                },
                "apellido_cli": {
                  required: "Debe ingresar el apellido",
                  minlength: "El apellido debe tener minimo 4 caracteres",
                  maxlength: "El apellido no puede tener mas de 15 caracteres"

                },
                "direccion_cli": {
                  required: "Debe ingresar la dirección",
                  minlength: "La dirección debe tener minimo 4 caracteres",
                  maxlength: "La dirección no puede tener mas de 20 caracteres"


                },
                "telefono_cli": {
                  required: "Por favor, ingrese el telefono",
                  minlength: "El telefono debe tener minimo 10 caracteres",
                  maxlength: "El telefono no puede tener mas de 10 caracteres"
                },
                "email_cli": {
                  required: "Por favor, ingrese el email",
                  email: "Por favor, ingresa un correo electrónico válido"

                },
                "estado_cli": {
                  required: "Debe ingresar el estado del cliente",


                }




              }
          });
      });


      </script>
