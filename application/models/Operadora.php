<?php
    class Operadora extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("operadora");

      return $respuesta;
  }


    //Consulta de datos
    function consultarTodos(){
      $operadoras=$this->db->get("operadora");
      if ($operadoras->num_rows()>0) {
        return $operadoras->result();
      } else {
        return false;
      }
    }


function obtenerPorId($id_ope)
{function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("operadora");

      return $respuesta;
  }
    $this->db->where("id_ope", $id_ope);
    $operadora = $this->db->get("operadora");
    if ($operadora->num_rows() > 0) {
        return $operadora->row();
    } else {
        return false;
    }
}



    function eliminar($id_ope){
        $this->db->where("id_ope",$id_ope);
        return $this->db->delete("operadora");
    }

function actualizar($id_ope,$datos){
  $this->db->where("id_ope",$id_ope);
  return $this->db->update("operadora",$datos);
}


  }//Fin de la clase
?>
