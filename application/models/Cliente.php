<?php
  class Cliente extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("cliente",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $clientes=$this->db->get("cliente");
      if ($clientes->num_rows()>0) {
        return $clientes->result();
      } else {
        return false;
      }
    }

    public function consultarTodosConOperadora() {
    $this->db->select('cliente.*, operadora.nombre_ope AS nombre_ope');
    $this->db->from('cliente');
    
    $this->db->join('operadora', 'cliente.fk_id_ope = operadora.id_ope', 'left');
    $query = $this->db->get();
    return $query->result();
}


    // Obtener hospital por ID
function obtenerPorId($id_cli)
{
    $this->db->where("id_cli", $id_cli);
    $cliente = $this->db->get("cliente");
    if ($cliente->num_rows() > 0) {
        return $cliente->row();
    } else {
        return false;
    }
}



    //eliminacion de hospital por id
    function eliminar($id_cli){
        $this->db->where("id_cli",$id_cli);
        return $this->db->delete("cliente");
    }

    //funcion para actualizar hospitales
function actualizar($id_cli,$datos){
  $this->db->where("id_cli",$id_cli);
  return $this->db->update("cliente",$datos);
}







   function obtenerListadoOperadoras()
   {
       $operadoras = $this->db->get("operadora")->result();
       return $operadoras;
   }



  }//Fin de la clase



?>
